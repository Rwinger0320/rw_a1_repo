var player1 = {
    playerName: "Player1",
    btnLocation: [],
    arrayAt: 0,
    img: "img/x.png"
}

var player2 = {
    playerName: "Player2",
    btnLocation: [],
    arrayAt: 0,
    img: "img/o.png"
}

var usedBtns = {

};

var clickedBtn = "";//string
var btnImage = {};//object

var playerWin = false;


var aiInfo ={
  availableButtons:{

  },
  playerName: "CPU1",
  btnLocation: [],
  arrayAt: 0,
  img: "img/o.png",
  diff : {
      medium:{
        piecesOpponentChooses: [],
        counter: 0,
        opponentBoardWinningPieces: {
          w1:{
            group1:{
              btn01: false,
              btn02: false,
              btn03: false,
            },
            group2:{
              btn01: false,
              btn05: false,
              btn09: false,
            },
            group3:{
              btn01: false,
              btn04: false,
              btn07: false
            }
          },
          w2:{
            group1:{
              btn02: false,
              btn05: false,
              btn08: false,
            }
          },
          w3:{
            group1:{
              btn03: false,
              btn06: false,
              btn09: false,
            },
            group2:{
              btn03: false,
              btn05: false,
              btn07: false,
            }
          },
          w4:{
            group1:{
              btn04: false,
              btn05: false,
              btn06: false
            }
          },
          w5:{
            group1:{
              btn07: false,
              btn08: false,
              btn09: false
            }
          }
        },
        comboCount:{
          w1: [[0, false],[0, false],[0, false]],
          w2: [[0, false]],
          w3: [[0, false],[0, false]],
          w4: [[0, false]],
          w5: [[0, false]]

        }
      },
      hard:{
        comboCountAI:{
          w1: [[0, false],[0, false],[0, false]],
          w2: [[0, false]],
          w3: [[0, false],[0, false]],
          w4: [[0, false]],
          w5: [[0, false]]
        }
      }
  }
}

var boardPieceWinningCombos = {
  btn01:[["btn02", "btn03"], ["btn05", "btn09"], ["btn04", "btn07"]],
  btn02:[["btn05", "btn08"]],
  btn03:[["btn06", "btn09"], ["btn05", "btn07"]],
  btn04:[["btn05", "btn06"]],
  btn07:["btn08", "btn09"]
}

var aiDiff = "";
