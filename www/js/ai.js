var openSlots = new Object();

function createOpenAiSlot(){
  for(var i = 1; i < 10; i++){
    var slotName = "btn0" + i;
    aiInfo.availableButtons[slotName] = "true"
  }
  //console.log(aiInfo.availableButtons);
}

//this is only used when int he hard difficulty
function OpponentWinState(){
  var boardPieceObject = aiInfo.diff.medium.comboCount;
  var isFound = false;
  for(key in boardPieceObject){
    var newObject = boardPieceObject[key];
    for(var i = 0; i < boardPieceObject[key].length; i++){
      if(boardPieceObject[key][i][0] == 2 && boardPieceObject[key][i][1] == false){
        return true
        break;
      }
    }
    if(isFound == true){
      break;
    }
  }

  if(isFound == false){
      return false;
  }

}

//this function is used when in the hard difficulty
function AICheckWinnCombo(){
  var boardPieceObject = aiInfo.diff.hard.comboCountAI;
  var isFound = false;
  for(key in boardPieceObject){
    var newObject = boardPieceObject[key];
    for(var i = 0; i < boardPieceObject[key].length; i++){
      if(boardPieceObject[key][i][0] == 2 && boardPieceObject[key][i][1] == false){
        isFound = true;
        AIFindWinBtn(key, i, "yes");
        console.log("winning")
        return true;
        break;
      }
    }
      if(isFound == true){
        break;
      }
    }

    if (isFound == false){
      return false;
    }
  }

  function AICheckForNextButton(){
    console.log("finding next button");
    var boardPieceObject = aiInfo.diff.hard.comboCountAI;
    var isFound = false;
    for(key in boardPieceObject){
      var newObject = boardPieceObject[key];
        for(var i = 0; i < boardPieceObject[key].length; i++){
          if(boardPieceObject[key][i][0] == 1 && boardPieceObject[key][i][1] == false){
            isFound = true;
            console.log("found button to press");
            AIFindWinBtn(key, i, "no");
            // return true;
            break;
          }
        }
      if(isFound == true){
        break;
      }

    }

  //this is where we would add the winning functionality to our AI (only if in hard mode which has not been built yet)
  if(isFound == false){
    return false;
  }
  else{
    return true;
  }

}

function AIFindWinBtn(key, group, didWin){
  var group;
  switch(group){
    case 0:
      group = "group1";
      break;
    case 1:
      group = "group2";
      break;
    case 2:
      group = "group3";
      break;
  }

  var boardPieceObject = aiInfo.diff.medium.opponentBoardWinningPieces[key][group];
  for(key in boardPieceObject){
    if(boardPieceObject[key] == false){
        console.log("found button to press");
        AISetButton(key, didWin);
        break;
    }
  }
}

function AISetButton(key, didWin){
  var btnName = key;
  clickedBtn = key;

  console.log("In AISetButton, and is win == " + didWin);
  // usedBtns[btnName] = true;
  setPlayerBtn(aiInfo, btnName);
  if(didWin == "yes"){
    setTimeout(function(){
      alert(aiInfo.playerName + " WINS!");
      $("#tic_tac_table").html("");
      resetPlayers();

      generateGame();
     }, 300);
  }
  else{
    checkAIChoice();
    changePlayer();
  }


}

function AICheckBlockPlayer(){
    var boardPieceObject = aiInfo.diff.medium.comboCount;
    var isFound = false;
    for(key in boardPieceObject){
      var newObject = boardPieceObject[key];
      for(var i = 0; i < boardPieceObject[key].length; i++){

        if(boardPieceObject[key][i][0] == 2 && boardPieceObject[key][i][1] == false){
          isFound = true;
          boardPieceObject[key][i][1] = true;
          if(aiDiff == "hard"){
            aiInfo.diff.hard.comboCountAI[key][i][1] = true;
          }
          console.log("Inside AICheckBlockPlayer Function");
          console.log("The key is: " + key);
          console.log("I is: " + i);
          AIFindBtn(key, i);
          if(aiDiff == "hard"){
            return true;
          }
          break;
        }
      }
      if(isFound == true){
        break;
      }

    }

    //this is where we would add the winning functionality to our AI (only if in hard mode which has not been built yet)
    if(isFound == false){
      if(aiDiff == "hard"){
        return false
      }
      else{
        AIChooseCell();
      }
    }
}


function AIFindBtn(key, group){
  console.log("the key to finding the button is: " + key);
  console.log("the group is: " + group);
    var group;
    switch(group){
      case 0:
        group = "group1";
        break;
      case 1:
        group = "group2";
        break;
      case 2:
        group = "group3";
        break;
    }

    var boardPieceObject = aiInfo.diff.medium.opponentBoardWinningPieces[key][group];
    for(key in boardPieceObject){
      if(boardPieceObject[key] == false){
          AIBlockPlayer(key);
          break;
      }
    }
}

function AIBlockPlayer(key){
  var btnName = key;
  clickedBtn = key;
  // usedBtns[btnName] = true;
  setPlayerBtn(aiInfo, btnName);
  checkAIChoice();
  changePlayer();
}

function AIChooseCell(){
  var key = chooseRanNum();
  var btnName = key;
  clickedBtn = key;
  // usedBtns[btnName] = true;
  setPlayerBtn(aiInfo, btnName);
  if(aiDiff == "medium" || aiDiff == "hard"){
    checkAIChoice();
  }

  changePlayer();
}

function checkAIChoice(){
  //keeps track of the winning combos for the opponent
  var boardPieceObject = aiInfo.diff.medium.opponentBoardWinningPieces;

  var counter = 0;
  var group = 0;
  for(key in boardPieceObject){

    var newObject = boardPieceObject[key]
    for(key2 in newObject){
      if(newObject[key2][clickedBtn] == false){
        // newObject[key2][clickedBtn] = true;
        disableCombo(key, key2);
      }
    }

  }
}

function disableCombo(comboWinNum, comboGroup){
  var pos;
  switch(comboGroup){
    case "group1":
       pos = 0;
       break;
    case "group2":
       pos = 1;
       break;
    case "group3":
       pos = 2;
       break;
  }

  //console.log(pos);

  aiInfo.diff.medium.comboCount[comboWinNum][pos][1] = true;
  if(aiDiff == hard){
    aiInfo.diff.hard.comboCountAI[comboWinNum][pos][1] = true;
  }
}

//used when ai is set to easy
function chooseRanNum(){
  var myNum = Math.floor(Math.random() * Object.keys(aiInfo.availableButtons).length) + 1;

  //console.log(myNum);

  var counter = 1;
  for(key in aiInfo.availableButtons){
    if(counter == myNum){
      return key;
    }
    counter++;
  }

}



function removeBtnAvail(){
  delete aiInfo.availableButtons[clickedBtn];
}

function keepTrackOfAI(){
  var boardPieceObject = aiInfo.diff.medium.opponentBoardWinningPieces;

  var counter = 0;
  var group = 0;
  for(key in boardPieceObject){

    var newObject = boardPieceObject[key];
    for(key2 in newObject){
      if(newObject[key2][clickedBtn] == false){
        newObject[key2][clickedBtn] = true;
        changeCountAI(key, key2);
      }
    }

  }

}

function changeCountAI(comboWinNum, comboGroup){
  var pos;
  switch(comboGroup){
    case "group1":
       pos = 0;
       break;
    case "group2":
       pos = 1;
       break;
    case "group3":
       pos = 2;
       break;
  }

  aiInfo.diff.hard.comboCountAI[comboWinNum][pos][0] = aiInfo.diff.hard.comboCountAI[comboWinNum][pos][0] + 1;
  if(aiInfo.diff.hard.comboCountAI[comboWinNum][pos][0] == 1){
    aiInfo.diff.medium.comboCount[comboWinNum][pos][1] = true;
    //aiInfo.diff.hard.comboCountAI[comboWinNum][pos][1] = true;
  }
}

function keepTrackOfPlayer(){
  //keep track of buttons clicked
  aiInfo.diff.medium.piecesOpponentChooses[aiInfo.diff.medium.counter] = clickedBtn;
  aiInfo.diff.medium.counter = aiInfo.diff.medium.counter + 1;

  //keeps track of the winning combos for the opponent
  var boardPieceObject = aiInfo.diff.medium.opponentBoardWinningPieces;

  var counter = 0;
  var group = 0;
  for(key in boardPieceObject){

    var newObject = boardPieceObject[key]
    for(key2 in newObject){
      if(newObject[key2][clickedBtn] == false){
        newObject[key2][clickedBtn] = true;
        changeCount(key, key2);
      }
    }

  }

}

function changeCount(comboWinNum, comboGroup){
   var pos;
   switch(comboGroup){
     case "group1":
        pos = 0;
        break;
     case "group2":
        pos = 1;
        break;
     case "group3":
        pos = 2;
        break;
   }

   aiInfo.diff.medium.comboCount[comboWinNum][pos][0] = aiInfo.diff.medium.comboCount[comboWinNum][pos][0] + 1;
   if(aiDiff == "hard"){
     //this deactivates a combo when the player has chosen a section
      if(aiInfo.diff.medium.comboCount[comboWinNum][pos][0] == 1){
        aiInfo.diff.hard.comboCountAI[comboWinNum][pos][1] = true;
      }
   }
}
