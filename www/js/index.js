var turn = 0;
var aiOn = false;

$(document).ready(function(){
  createOpenAiSlot();

  setTimeout(function(){
    $("#myModal").css("display", "block");
  }, 500);

  $(".selectBtn").on("click", function(){
    if($("#startGame").is(":visible") == false){
      $("#startGame").show();
    }

    if($(this).attr("id") == "CPU"){
      if(aiOn == false){
        aiOn = true;
      }
      displayCPUOptions();
    }
    else if($(this).attr("id") == "PLAYER"){
      if(aiOn == true){
        aiOn = false;
      }
      displayPlayerOptions();
    }
  })

  $(".diffBtn button").on("click", function(){
    console.log($(this).attr("id"));
    setAIDiff($(this).attr("id"));
  });

  $("#startGame").on("click", function(){
      $("#myModal").css("display", "none");
    setTimeout(function(){
      generateGame();
    }, 500);
  });
  //generateGame();
});

function setAIDiff(diff){
  var elementId = "#" + diff;
  if(aiDiff == ""){
    aiDiff = diff;
    $(elementId).css("background-color", "#6497b1");
    $(elementId).css("color", "white");
  }
  else{
      var oldAIDiff = "#" + aiDiff;
      $(oldAIDiff).css("background-color", "");
      $(oldAIDiff).css("color", "black");
      aiDiff = diff;
      $(elementId).css("background-color", "#6497b1");
      $(elementId).css("color", "white");
  }


}

function displayCPUOptions(){
  if($(".CPUCHOICE").is(":visible") == false){
    $(".CPUCHOICE").show();
  }
}

function displayPlayerOptions(){
  if($(".CPUCHOICE").is(":visible") == true){
    $(".CPUCHOICE").hide();
  }
}

function generateGame(){
  createBoard();

  var btnAt = 1;
  for(var i = 0; i < 9; i++){
    btnAtName = "btn0" + btnAt;

    aiInfo.availableButtons[btnAtName] = true;
    btnAt = btnAt + 1;
  }

  $("#PlayerTurn").html(player1.playerName + " TURN");
  $(".tic_tac_button").on("click", function(){
    // console.log();
    // console.log($(this).children("img").attr("src"));
    clickedBtn = $(this).attr("id");
    btnImage = $(this).children("img");
    getPlayer();
  })

}

function createBoard(){
  var tableHtml = ""
  var btnNumber = 1;
  var trOn = "even";
  var tdOn = 0;

  for(var i = 0; i < 5; i++){
    if(trOn == "even"){
      tdOn = 0;
      tableHtml += "<tr>";
      for(var x = 0; x < 5; x++){
        if(tdOn == 0){
          var divId = "btn0" + btnNumber;
          tableHtml += "<td><div class='tic_tac_button' id='" + divId + "'><img class='imgShown'/></div></td>";
          btnNumber = btnNumber + 1;
          tdOn = 1;
        }
        else{
          tableHtml += "<td><img class='vertical_line' src='img/line_vertical.png'/></td>";
          tdOn = 0;
        }
      }

      tableHtml += "<tr>";
      trOn = "odd";

    }
    else{
      tableHtml += "<tr class='horTR'> <td colspan='5'><img class='horizontal_line' src='img/line_horizontal.png'/></td> </tr>";
      trOn = "even";
    }
  }

  $("#tic_tac_table").html(tableHtml);


}

function getPlayer(){
  var player = new Object();
  switch(turn){
    case 0:
      player = player1;
      checkBtn(player);
      break;
    case 1:
      player = player2;
      checkBtn(player);
      break;
  }
}

function checkBtn(player, btnLocation){
  // alert(Object.keys(usedBtns).length);
  if(Object.keys(usedBtns).length == 0){
      setPlayerBtn(player, btnLocation);
      changePlayer();
  }
  else{
    if(usedBtns[clickedBtn] == undefined || usedBtns[clickedBtn] == ""){
      setPlayerBtn(player, btnLocation);
      isFound = false;
      if(Object.keys(usedBtns).length > 2){
          playerWin = checkPlayerBtns(player);
          if(playerWin == true){
            setTimeout(function(){
               displayUserWinner(player)
             }, 300);
          }
      }
      if(playerWin == false){
        if(Object.keys(usedBtns).length == 9){
          setTimeout(function(){
             alert("No One Wins!");
             resetPlayers();
             generateGame();
           }, 300);
        }
        else{
          changePlayer();
        }
      }
    }
    else{
      alert("Some One has already picked that button, try again");
    }

  }
}

function changePlayer(){
  switch(turn){
    case 0:
      turn = 1;
      if(aiOn == true){
        //Call ai function to choose pixel
          $("#PlayerTurn").html(aiInfo.playerName + " TURN");
          setTimeout(function(){
            if(aiDiff == "medium"){
              AICheckBlockPlayer();
            }
            else if(aiDiff == "hard"){
              //see if AI has won
              var isFound = AICheckWinnCombo();
              //if AI has not won then
              if(isFound == false){
                console.log("");
                console.log("This is a new pic");
                console.log("No winning Combo");
                var isBlockFound = AICheckBlockPlayer();
                console.log(isBlockFound);
                //if the player is not going to win then
                if(isBlockFound == false){
                  console.log("checking for next button inside no winning combo");
                   var comboButtonFound = AICheckForNextButton();
                   //if the AI has yet to make a move then
                   if(comboButtonFound == false){
                     console.log("finding random button");
                     AIChooseCell();
                   }
                 }
                 else{
                   console.log("no block found");
                   // AIChooseCell();
                   //AICheckForNextButton();
                 }

              }

            }
            else{
               AIChooseCell();
            }

           }, 1000);
      }
      else{
          $("#PlayerTurn").html(player2.playerName + " TURN");
      }


      break;
    case 1:
      turn = 0;
      $("#PlayerTurn").html(player1.playerName + " TURN");

      break;
  }
}

function setPlayerBtn(player, btnLocation){
  console.log("The buttonlocation is: " + btnLocation);
  usedBtns[clickedBtn] = true;

  player.btnLocation[player.arrayAt] = clickedBtn;
  player.arrayAt = player.arrayAt + 1;
  var element = "#" + clickedBtn;

  if(aiOn == true){
    removeBtnAvail();

    if(player.playerName != aiInfo.playerName && (aiDiff == "medium" || aiDiff == "hard")){
      keepTrackOfPlayer();
    }
    else if (aiDiff == "hard"){
      keepTrackOfAI();
    }

    btnImage = $(element).children("img");

  }

  btnImage.attr("src", player.img);
  // $(btnLocation).css("background-image", url(player.img));

}

function checkPlayerBtns(player){

  var playerBtnArray = player.btnLocation;

  if(playerBtnArray.indexOf("btn01") > -1){
      if(playerBtnArray.indexOf("btn02") > -1 && playerBtnArray.indexOf("btn03") > -1){
        //winner
        return true;
      }
      else if(playerBtnArray.indexOf("btn05") > -1 && playerBtnArray.indexOf("btn09") > -1){
        //winner
        return true;
      }
      else if(playerBtnArray.indexOf("btn04") > -1 && playerBtnArray.indexOf("btn07") > -1){
        //winner
        return true;
      }
  }

  if(playerBtnArray.indexOf("btn02") > -1){
    if(playerBtnArray.indexOf("btn05") > -1 && playerBtnArray.indexOf("btn08") > -1){
      //winner
      return true;
    }
  }

  if(playerBtnArray.indexOf("btn03") > -1){
    if(playerBtnArray.indexOf("btn06") > -1 && playerBtnArray.indexOf("btn09") > -1){
      //winner
      return true;
    }
    else if(playerBtnArray.indexOf("btn05") > -1 && playerBtnArray.indexOf("btn07") > -1){
      //winner
      return true;
    }

  }
  if(playerBtnArray.indexOf("btn04") > -1){
    if(playerBtnArray.indexOf("btn05") > -1 && playerBtnArray.indexOf("btn06") > -1){
      //winner
      return true;
    }
  }
  if(playerBtnArray.indexOf("btn07") > -1){
    if(playerBtnArray.indexOf("btn08") > -1 && playerBtnArray.indexOf("btn09") > -1){
      //winner
      return true;
    }
  }

  return false;

}

function resetPlayers(){
  player1.btnLocation = [];
  player1.arrayAt = 0;
  player2.btnLocation = [];
  player2.arrayAt = 0;

  usedBtns = {};
  clickedBtn = "";
  btnImage = {};
  playerWin = false;
  turn = 0;

  if(aiOn == true){
    aiInfo.availableButtons = {};
    aiInfo.btnLocation = [];
    aiInfo.arrayAt = 0;

    if(aiDiff == "medium" || aiDiff == "hard"){
        aiInfo.diff.medium.piecesOpponentChooses = [];
        aiInfo.diff.medium.counter = 0;
        var object = aiInfo.diff.medium.opponentBoardWinningPieces;
        for(key in object){
          var newObject = object[key];
          for(key2 in newObject){
            var groupObject = newObject[key2];
            for(key3 in groupObject){
              groupObject[key3] = false;
            }
          }
        }

        var object = aiInfo.diff.medium.comboCount;

        for(key in object){
          var newObject = object[key];
          for(var i = 0; i < Object.keys(newObject).length; i++){
            newObject[i][0] = 0;
            newObject[i][1] = false;
          }
        }

        if(aiDiff == "hard"){
          var object = aiInfo.diff.hard.comboCountAI;

          for(key in object){
            var newObject = object[key];
            for(var i = 0; i < Object.keys(newObject).length; i++){
              newObject[i][0] = 0;
              newObject[i][1] = false;
            }
          }
        }
    }

  }

}

function displayUserWinner(player){
  alert(player.playerName + " WINS!");
  $("#tic_tac_table").html("");
  resetPlayers();

  generateGame();
}
